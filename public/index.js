
var app = angular.module("formApp",[]);

app.controller("FormController",['$scope', function($scope) {
    $scope.data = [];
   
    
    $scope.addNewQuestion = function(){
        var question = {
            text : "",
            type : "",
            options : []
        }
        $scope.data.push(question);
    }

    $scope.addOption = function(question){
        var option = {
            text : ""
        }
        question.options.push(option)
        console.log($scope.data);
        
    }

    $scope.removeOption = function(question,option){
        question.options.splice(question.options.indexOf(option),1);
    }


}])